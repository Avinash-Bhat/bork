package com.bhatworks.clirpg.core.ui.text

import org.hamcrest.Matchers.equalTo
import org.hamcrest.comparator.ComparatorMatcherBuilder
import org.junit.Assert.assertThat
import org.junit.Test

class TextTest {

    val comparatorMatch = ComparatorMatcherBuilder.comparedBy(visualComparator)!!

    @Test
    fun testCompareTo_sameTextDifferentStyle_bothBottom() {
        val text1 = bottomText { "abc" }
        val text2 = bottomText { center = true; "abc" }
        with(comparatorMatch) {
            assertThat(text1, comparesEqualTo(text2))
            assertThat(text2, comparesEqualTo(text1))
        }
    }

    @Test
    fun testCompareTo_sameTextDifferentStyle() {
        val text1 = bottomText { "abc" }
        val text2 = centerText { "abc" }
        with(comparatorMatch) {
            assertThat(text1, greaterThan(text2))
            assertThat(text2, lessThan(text1))
        }
    }

    @Test
    fun testCompareTo_sameStyleDifferentText() {
        val text1 = bottomText { "abc" }
        val text2 = bottomText { "abcd" }
        with(comparatorMatch) {
            assertThat(text1, comparesEqualTo(text2))
        }
    }

    @Test
    fun testCompareTo_sameStyleSameText() {
        val text1 = bottomText { "abc" }
        val text2 = bottomText { "abc" }
        with(comparatorMatch) {
            assertThat(text1, comparesEqualTo(text2))
            assertThat(text2, comparesEqualTo(text1))
        }
    }

    @Test
    fun testCompareTo_bySort_random() {
        val t1 = text { "abc" }
        val t2 = text { "abcd" }
        val t3 = text { "abcde" }
        val t4 = bottomText { "abc" }
        val t5 = bottomText { center = true;"abc" }
        val t6 = bottomText { "abcd" }
        val t7 = bottomText { center = true; "abcd" }
        val t8 = bottomText { "abcde" }
        val t9 = bottomText { center = true; "abcde" }
        val input = listOf(t3, t5, t4, t7, t9, t8, t6, t1, t2)
        val expected = listOf(t3, t1, t2, t5, t4, t7, t9, t8, t6)
        assertThat(input.sortedWith(visualComparator), equalTo(expected))
    }

    @Test
    fun testCompareTo_bySort_reversed() {
        val t1 = text { "abc" }
        val t2 = text { "abcd" }
        val t3 = text { "abcde" }
        val t4 = bottomText { "abc" }
        val t5 = bottomText { center = true; "abc" }
        val t6 = bottomText { "abcd" }
        val t7 = bottomText { center = true; "abcd" }
        val t8 = bottomText { "abcde" }
        val t9 = bottomText { center = true; "abcde" }
        val input = listOf(t9, t8, t7, t6, t5, t4, t3, t2, t1)
        val expected = listOf(t3, t2, t1, t9, t8, t7, t6, t5, t4)
        assertThat(input.sortedWith(visualComparator), equalTo(expected))
    }

    @Test
    fun testCompareTo_bySort_realcase() {
        val t1 = centerText { "Welcome to the Game" }
        val t2 = bottomText { center = true; "Press enter key to start" }
        val t3 = bottomText { ">" }
        val input = listOf(t1, t2, t3)
        val expected = listOf(t1, t2, t3)
        assertThat(input.sortedWith(visualComparator), equalTo(expected))
    }
}
