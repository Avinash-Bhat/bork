package com.bhatworks.clirpg

import com.bhatworks.clirpg.core.exception.GameException
import com.bhatworks.clirpg.game.Game

fun main(args: Array<String>) {
    val game = Game()
    try {
        game.run()
    } catch (e: GameException) {
        println("ERROR: ${e.message}")
        e.printStackTrace(System.err)
        System.exit(-1)
    } finally {
        game.stop()
    }
}