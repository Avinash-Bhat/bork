package com.bhatworks.clirpg.game

import com.bhatworks.clirpg.core.console.Console
import com.bhatworks.clirpg.core.console.UnixSystemConsole
import com.bhatworks.clirpg.core.console.WindowsSystemConsole
import com.bhatworks.clirpg.core.engine.GameEngine
import com.bhatworks.clirpg.core.engine.TextRPGEngine
import com.bhatworks.clirpg.game.screen.EndScreen
import com.bhatworks.clirpg.game.screen.GameScreen
import com.bhatworks.clirpg.game.screen.MainScreen

class Game : Runnable {
    private val console: Console by lazy {
        when {
            System.getProperty("os.name").toLowerCase().startsWith("windows") -> WindowsSystemConsole()
            else -> UnixSystemConsole()
        }
    }
    private val engine: GameEngine by lazy {
        TextRPGEngine(console)
    }

    override fun run() {
        engine.addScreen(MainScreen())
        engine.addScreen(GameScreen())
        engine.addScreen(EndScreen())
        engine.start()
    }

    fun stop() {
        engine.stop()
        console.close()
    }
}
