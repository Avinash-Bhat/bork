package com.bhatworks.clirpg.game.screen

import com.bhatworks.clirpg.core.ui.canvas.Canvas
import com.bhatworks.clirpg.core.ui.content.content
import com.bhatworks.clirpg.core.ui.text.centerText

class EndScreen : DecoratedScreen() {

    override fun draw(canvas: Canvas, state: Any) {
        canvas.overwriteMode()
        canvas.content = content {
            +centerText { "You have completed the game" }
        }
    }

    override fun process(command: String?, state: Any): Any {
        processed = true
        return state
    }

    override fun needPrompt(state: Any) = false

}
