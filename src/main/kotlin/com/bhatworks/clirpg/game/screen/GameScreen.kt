package com.bhatworks.clirpg.game.screen

import com.bhatworks.clirpg.core.ui.canvas.Canvas
import com.bhatworks.clirpg.core.ui.content.content
import com.bhatworks.clirpg.core.ui.text.inlineText
import com.bhatworks.clirpg.core.ui.text.text

class GameScreen : DecoratedScreen() {
    override fun draw(canvas: Canvas, state: Any) {
        val prompt: String
        when (state) {
            "at gate" -> {
                canvas.appendMode()
                prompt = "You are in front of the gate. Its closed!"
            }
            else -> {
                canvas.overwriteMode()
                prompt = "You are in a marsh land. You can see a gate at a distance what do you do?"
            }
        }
        if (state != "at gate") {
        }
        canvas.content = content {
            +text { prompt }
            +inlineText { ">" }
        }
    }

    override fun process(command: String?, state: Any): Any {
        return when (command) {
            "go towards the gate", "go to gate" -> "at gate"
            "open", "open gate" -> processed = true
            else -> state
        }
    }

}
