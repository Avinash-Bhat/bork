package com.bhatworks.clirpg.game.screen

import com.bhatworks.clirpg.core.ui.canvas.Canvas
import com.bhatworks.clirpg.core.ui.content.content
import com.bhatworks.clirpg.core.ui.text.bottomText
import com.bhatworks.clirpg.core.ui.text.centerText

class MainScreen : DecoratedScreen() {

    override fun draw(canvas: Canvas, state: Any) {
        canvas.content = content {
            +centerText {
                "Welcome to the Game"
            }
            +bottomText {
                center = true
                "Press enter key to start"
            }
            +bottomText {
                blockText = false
                ">"
            }
        }
    }

    override fun process(command: String?, state: Any): Any {
        processed = true
        return state
    }

}