package com.bhatworks.clirpg.game.screen

import com.bhatworks.clirpg.core.console.Console
import com.bhatworks.clirpg.core.ui.canvas.Canvas
import com.bhatworks.clirpg.core.ui.canvas.DrawMode.OVERWRITE
import com.bhatworks.clirpg.core.ui.screen.SimpleScreen
import com.bhatworks.clirpg.core.ui.text.Style
import com.bhatworks.clirpg.core.ui.text.Text

abstract class DecoratedScreen : SimpleScreen() {
    private val canvas = Canvas()

    final override fun draw(console: Console, state: Any) {
        draw(canvas, state)
        if (canvas.drawMode() == OVERWRITE) {
            console.clear()
        }
        val elements = canvas.content.elements
        val bottomElements = elements.filter { it.style.bottom }
        val normalElements = elements.filter { !it.style.bottom }
        val (_, maxY) = console.dimensions
        val bottomElementCount = bottomElements.count()
        normalElements.forEach(print(console))
        if (bottomElementCount > 0) {
            if (normalElements.size < (maxY - bottomElementCount)) {
                console.print("\n".repeat(maxY - bottomElementCount - 1))
            }
            bottomElements.forEach(print(console))
        }
    }

    private fun print(console: Console): (Text) -> Unit = { (text, style) ->
        val styledText = centerStyled(text, style, console.dimensions.first)
        when {
            style.blockText -> console.println(styledText)
            else -> console.print(styledText)
        }
    }

    internal fun centerStyled(text: String, style: Style, maxX: Int) = when {
        style.center -> "${" ".repeat(maxX / 2 - text.length / 2)}$text"
        else -> text
    }

    abstract fun draw(canvas: Canvas, state: Any)
}
