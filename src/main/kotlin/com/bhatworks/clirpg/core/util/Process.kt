package com.bhatworks.clirpg.core.util

import java.util.stream.Collectors

fun exec(vararg command: String, needOutput: Boolean = true, inheritIO: Boolean = true): String? {
    with(ProcessBuilder(*command)) {
        if (inheritIO) {
            inheritIO()
        }
        if (needOutput) {
            redirectOutput(ProcessBuilder.Redirect.PIPE)
        }
        val process = start()
        val out = process.inputStream.bufferedReader().lines().collect(Collectors.joining("\n"))
        process.waitFor()
        return out
    }
}