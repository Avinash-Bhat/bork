package com.bhatworks.clirpg.core.console

import com.bhatworks.clirpg.core.util.exec

class WindowsSystemConsole : SystemConsole() {
    override fun clear(): WindowsSystemConsole {
        exec("cmd", "/c", "cls")
        return this
    }

    override val dimensions: Pair<Int, Int> by lazy {
        // TODO find a way to get the terminal size without using jni
        80 to 45
    }
}
