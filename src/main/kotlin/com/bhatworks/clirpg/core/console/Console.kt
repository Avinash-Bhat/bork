package com.bhatworks.clirpg.core.console

interface Console {
    fun clear(): Console

    fun print(ch: Char): Console
    fun print(str: String): Console
    fun print(chs: CharArray): Console
    fun printf(str: String, vararg args: Any?): Console
    fun println(str: String): Console

    fun read(): Int
    fun readLine(): String
    fun close()

    val dimensions : Pair<Int, Int>
}
