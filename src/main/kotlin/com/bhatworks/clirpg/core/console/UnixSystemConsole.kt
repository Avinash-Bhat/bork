package com.bhatworks.clirpg.core.console

import com.bhatworks.clirpg.core.util.exec

class UnixSystemConsole : SystemConsole() {
    override fun clear(): UnixSystemConsole {
        with(System.out) {
            print("\u001b[2J\u001b[H");
            flush();
        }
        return this
    }

    override val dimensions: Pair<Int, Int> by lazy {
        val cols = exec("tput", "cols") ?: "80"
        val lines = exec("tput", "lines") ?: "45"
        cols.toInt() to lines.toInt()
    }
}
