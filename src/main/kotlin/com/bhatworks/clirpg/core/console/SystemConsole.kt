package com.bhatworks.clirpg.core.console

import java.util.*

abstract class SystemConsole() : Console {

    private var input = System.`in`
    private var scanner = Scanner(System.`in`)
    private val out = System.out

    override fun print(ch: Char): SystemConsole {
        out.print(ch)
        return this
    }

    override fun print(chs: CharArray): SystemConsole {
        out.print(chs)
        return this
    }

    override fun print(str: String): SystemConsole {
        out.print(str)
        return this
    }

    override fun println(str: String): SystemConsole {
        out.println(str)
        return this
    }

    override fun printf(str: String, vararg args: Any?): SystemConsole {
        out.printf(str, args)
        return this
    }

    override fun read() = input.read()
    override fun readLine() = scanner.nextLine()!!

    override fun close() = scanner.close()

}