package com.bhatworks.clirpg.core.exception

class InvalidGameStateException : GameException {
    constructor(message: String) : super(message) {}

    constructor(message: String, cause: Throwable) : super(message, cause) {}
}
