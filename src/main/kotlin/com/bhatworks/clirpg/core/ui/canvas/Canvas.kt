package com.bhatworks.clirpg.core.ui.canvas

import com.bhatworks.clirpg.core.ui.content.Content

class Canvas {
    private var drawMode = DrawMode.APPEND

    fun overwriteMode() {
        drawMode = DrawMode.OVERWRITE
    }

    fun appendMode() {
        drawMode = DrawMode.APPEND
    }

    fun drawMode() = drawMode

    var content: Content = Content(emptyList())
}

enum class DrawMode {
    APPEND, OVERWRITE
}
