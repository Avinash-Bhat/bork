package com.bhatworks.clirpg.core.ui.content

import com.bhatworks.clirpg.core.ui.text.Text
import com.bhatworks.clirpg.core.ui.text.visualComparator

data class Content(
        val elements: List<Text>
)

class ContentBuilder {
    var elements: MutableList<Text> = mutableListOf()
    operator fun Text.unaryPlus() {
        elements.add(this)
    }
}

fun content(init: ContentBuilder.() -> Unit): Content {
    val content = ContentBuilder()
    content.init()
    return Content(content.elements.toList().sortedWith(visualComparator))
}
