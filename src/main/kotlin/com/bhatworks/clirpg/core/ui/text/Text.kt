package com.bhatworks.clirpg.core.ui.text

data class Text(
        val text: String,
        val style: Style
)

val visualComparator: Comparator<Text> = Comparator { text, other ->
    with(text.style.bottom) {
        compareTo(other.style.bottom)
    }
}

data class Style(
        val bottom: Boolean = false
        , val center: Boolean = false
        , val blockText: Boolean = true
)

class TextBuilder {
    var bottom: Boolean = false
    var center: Boolean = false
    var blockText: Boolean = true
}

fun text(init: TextBuilder.() -> String): Text {
    val builder = TextBuilder()
    val text = builder.init()
    return Text(text, Style(center = builder.center, bottom = builder.bottom, blockText = builder.blockText))
}

fun centerText(init: TextBuilder.() -> String) = text {
    center = true
    init()
}

fun bottomText(init: TextBuilder.() -> String) = text {
    bottom = true
    init()
}
fun inlineText(init: TextBuilder.() -> String) = text {
    blockText = false
    init()
}