package com.bhatworks.clirpg.core.ui.screen

abstract class SimpleScreen : Screen {
    var processed = false

    override fun canProgress() = processed

    override fun needPrompt(state: Any) = true

}
