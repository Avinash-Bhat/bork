package com.bhatworks.clirpg.core.ui.screen

import com.bhatworks.clirpg.core.console.Console

interface Screen {
    fun canProgress(): Boolean
    fun draw(console: Console, state: Any)
    fun process(command: String?, state: Any): Any
    fun needPrompt(state: Any): Boolean
}
