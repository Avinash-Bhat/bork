package com.bhatworks.clirpg.core.engine

import com.bhatworks.clirpg.core.ui.screen.Screen

interface GameEngine {
    fun addScreen(screen: Screen)
    fun start()
    fun stop()
}
