package com.bhatworks.clirpg.core.engine

import com.bhatworks.clirpg.core.console.Console
import com.bhatworks.clirpg.core.ui.screen.Screen

class TextRPGEngine(private val console: Console) : GameEngine {
    private val screens: MutableList<Screen> = mutableListOf()
    var started = false;

    override fun start() {
        started = true
        var index = 0
        var state: Any = "";
        // start the game loop
        while (started && index < screens.size) {
            val screen = screens[index]
            screen.draw(console, state)
            val command = when {
                screen.needPrompt(state) -> console.readLine()
                else -> ""
            }
            state = screen.process(command, state)
            if (screen.canProgress()) {
                index++
            }
        }
    }

    override fun addScreen(screen: Screen) {
        when {
            started -> IllegalStateException("cannot add the screen after starting the engine")
            else -> screens.add(screen)
        }
    }

    override fun stop() {
        started = false
    }
}
